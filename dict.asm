%define addr_size 8

global find_word
extern string_equals

section .text

; rdi - строка \ rsi -начало словаря
; Если подходящее вхождение найдено, вернёт адрес начала вхождения в словарь (не значения), иначе вернёт 0.
find_word: 
        mov r8, rdi
        mov r9, rsi
    
    .loop_search:
        mov rdi, [r9]
        test rdi, rdi
        je .reached_end
        add rdi, addr_size
        mov rsi, r8

        push r8
        push r9
        call string_equals
        pop r9
        pop r8

        test rax, rax
        jne .success
        mov r9, [r9]
        jmp .loop_search

    .reached_end:
        xor rax, rax
        ret

    .success:
        mov rax, [r9]
        ret

