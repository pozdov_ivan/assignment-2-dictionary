#!/usr/bin/python3

import unittest
from subprocess import CalledProcessError, Popen, PIPE

class DictionaryTest(unittest.TestCase):

    def launch(self, input):
        output = b''
        try:
            p = Popen(['./main'], shell=None, stdin=PIPE, stdout=PIPE, stderr=PIPE)
            (output, errors) = p.communicate(input.encode())
            self.assertNotEqual(p.returncode, -11, 'segmentation fault')
            return (output.decode(), errors.decode())
        except CalledProcessError as exc:
            self.assertNotEqual(exc.returncode, -11, 'segmentation fault')
            return (exc.output.decode(), exc.returncode)

    def test_find_word(self):
        dic = {
            'bob_key': 'bob_value',
            'bab_key': 'bab_value',
            'bib_key': 'bib_value',
            'beb_key': 'beb_value',
            'bub_key': 'bub_value',
            'byb_key': 'byb_value'
        }
        inputs = ['bob_key', 'bab_key', 'bib_key', 'beb_key', 'bub_key','byb_key']
        for input in inputs:
            (output, errors) = self.launch(input)
            if (dic.get(input, "woke") != "woke"):
                self.assertEqual(output, dic.get(input), 'query: %s found: %s, expected: %s' % (repr(input), repr(output), repr(dic.get(input))))
            else:                        
                 self.assertEqual(errors, "woke",  'error from the program:  %s' % (repr(errors)))

if __name__ == "__main__":
    unittest.main()
