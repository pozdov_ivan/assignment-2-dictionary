ASM=nasm
ASMFLAGS=-f elf64
LD=ld
MAKE=make

main: main.o lib.inc words.inc dict.inc
	$(LD) -o main main.o lib.o dict.o
	$(MAKE) clean


%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

lib.inc: lib.o

dict.inc: dict.o

words.inc: colon.inc



.PHONY: clean
clean:
	rm *.o

.PHONY: test
test:
	python2 test.py
