%define buff_size 255
%define addr_size 8

%include "lib.inc"
%include "dict.inc"

section .data

%include "words.inc"

err_msg_not_found: db "no such key in the dict", 0
err_msg_too_long: db "the key is too long, try again", 0

section .bss

buffer: resb buff_size

section .text

_start:
        mov rdi, buffer
        mov rsi, buff_size
        call read_word ; адрес буфера в rax, длина слова в rdx. при неудаче 0 в rax
        test rax, rax
        je .redo

        mov rdi, rax
        mov rsi, dict_addr
        call find_word ; rdi - строка, rsi - начало словаря
        test rax, rax
        je .fail

    .success:
        mov rdi, rax
        add rdi, addr_size
        push rdi    ; посчитать длину значения
        call string_length
        pop rdi
        add rdi, rax
        inc rdi
        call print_string
        jmp .end

    .fail:
        mov rdi, err_msg_not_found
        call print_error
        jmp .end

    .redo:
        mov rdi, err_msg_too_long
        call print_error

    .end:
        xor rdi, rdi
        call exit